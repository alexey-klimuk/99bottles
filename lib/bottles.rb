class Bottles
  def song
    verses(99, 0)
  end

  def verses(b, a)
    b.downto(a).map { |i| verse(i) }.join("\n")
  end

  def verse(number)
    bn = BottleNumber.new(number)
    <<-VERSE
#{bn.remain_bottles.capitalize} of beer on the wall, #{bn.remain_bottles} of beer.
#{bn.action}, #{bn.left_bottles} of beer on the wall.
    VERSE
  end
end

class BottleNumber
  attr_reader :value
 
  def initialize(number)
    @value = number
  end

  def left_bottles
    left = value - 1
    return "99 bottles" if left < 0
    return "no more bottles" if left == 0
    return "1 bottle" if left == 1
    "#{left} bottles"
  end

  def remain_bottles
    return "no more bottles" if value <= 0
    return "#{value} bottle" if value == 1
    "#{value} bottles"
  end

  def action
    return "Go to the store and buy some more" if value <= 0
    return "Take it down and pass it around" if value == 1
    "Take one down and pass it around"
  end
end
